﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tkachenko.Roman.TaxiCity.Model;
using Tkachenko.Roman.TaxiCity.Model.DataAccess;
using Tkachenko.Roman.TaxiCity.UI.ViewModels;

namespace Tkachenko.Roman.TaxiCity.UI.Services {
    public class CustomerService : IService<Customer, CustomerViewModel> {
        private readonly IRepository<Customer> _repository;

        public CustomerService() {
            _repository = DataAccessContext.Instance.Customers;
        }

        public Customer MapToModel(CustomerViewModel viewModel) =>
            new Customer() {
                CustomerID = viewModel.CustomerID,
                Name = viewModel.Name
            };
        public IEnumerable<Customer> MapToModels(IEnumerable<CustomerViewModel> viewModels) =>
            viewModels.Select((CustomerViewModel vm) => MapToModel(vm));

        public CustomerViewModel MapToViewModel(Customer model) =>
            new CustomerViewModel() {
                CustomerID = model.CustomerID,
                Name = model.Name
            };
        public IEnumerable<CustomerViewModel> MapToViewModels(IEnumerable<Customer> models) =>
            models.Select((Customer m) => MapToViewModel(m));

        public CustomerViewModel Get(Guid id) => MapToViewModel(_repository.Get(id));
        public IEnumerable<CustomerViewModel> GetAll() => MapToViewModels(_repository.GetAll());
        public void Save(CustomerViewModel viewModel) => _repository.Save(MapToModel(viewModel));
        public void Remove(Guid id) => _repository.Remove(id);
        public void Update(Guid id, CustomerViewModel viewModel) => _repository.Update(id, MapToModel(viewModel));
    }
}
