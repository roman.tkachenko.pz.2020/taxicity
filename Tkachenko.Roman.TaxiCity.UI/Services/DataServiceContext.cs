﻿using System;
using System.Collections.Generic;
using System.Text;
using Tkachenko.Roman.TaxiCity.Model;
using Tkachenko.Roman.TaxiCity.UI.ViewModels;

namespace Tkachenko.Roman.TaxiCity.UI.Services {
    public class DataServiceContext {
        // data services
        private readonly IService<Customer, CustomerViewModel> _customerService;
        private readonly IService<Driver, DriverViewModel> _driverService;
        private readonly IService<Ride, RideViewModel> _rideService;

        // singleton implementation
        private static DataServiceContext _instance;
        public static DataServiceContext Instance {
            get {
                if (_instance == null) {
                    _instance = new DataServiceContext();
                }
                return _instance;
            }
        }

        // access to services
        public IService<Customer, CustomerViewModel> Customers => _customerService;
        public IService<Driver, DriverViewModel> Drivers => _driverService;
        public IService<Ride, RideViewModel> Rides => _rideService;

        private DataServiceContext() {
            _customerService = new CustomerService();
            _driverService = new DriverService();
            _rideService = new RideService();
        }
    }
}
