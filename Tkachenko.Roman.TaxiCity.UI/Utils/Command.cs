﻿using System;
using System.Windows.Input;

namespace Tkachenko.Roman.TaxiCity.UI.Utils {
    public class Command : ICommand {
        public Predicate<object> _canExecute;
        public Action<object> _execute;

        public event EventHandler CanExecuteChanged {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }

        public Command(Action<object> action) : this(null, action) { }
        public Command(Predicate<object> canExec, Action<object> exec) {
            _canExecute = canExec;
            _execute = exec;
        }

        public bool CanExecute(object parameter) {
            return (_canExecute == null) ?
                true :
                _canExecute(parameter);
        }

        public void Execute(object parameter) {
            _execute(parameter);
        }
    }
}
