﻿using System;
using System.Collections.Generic;
using System.Windows;
using Tkachenko.Roman.TaxiCity.Model;
using Tkachenko.Roman.TaxiCity.Model.DataAccess;
using Tkachenko.Roman.TaxiCity.UI.ViewModels;

namespace Tkachenko.Roman.TaxiCity.UI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App() {
            ClearAll();
            SaveMock();
            Window window = new MainWindow() { DataContext = new MainViewModel() };
            window.Show();
        }

        protected override void OnExit(ExitEventArgs args) {
            try {
                DataAccessContext.Instance.Save();
            } catch (Exception e) {
                base.OnExit(null);
                throw e;
            }
        }
        
        private void ClearAll() {
            DataAccessContext dac = DataAccessContext.Instance;

            foreach (Customer customer in dac.Customers.GetAll())
                dac.Customers.Remove(customer.CustomerID);
            foreach (Driver driver in dac.Drivers.GetAll())
                dac.Drivers.Remove(driver.DriverID);
            foreach (Ride ride in dac.Rides.GetAll())
                dac.Rides.Remove(ride.RideID);

            dac.Save();
        }

        private void SaveMock() {
            List<Customer> cMock = new List<Customer>() {
                new Customer() {
                    CustomerID = Guid.NewGuid(),
                    Name = "Bob"
                },
                new Customer() {
                    CustomerID = Guid.NewGuid(),
                    Name = "Alice"
                },
                new Customer() {
                    CustomerID = Guid.NewGuid(),
                    Name = "Charlie"
                },
                new Customer() {
                    CustomerID = Guid.NewGuid(),
                    Name = "Dave"
                },
                new Customer() {
                    CustomerID = Guid.NewGuid(),
                    Name = "Eve"
                }
            };
            List<Driver> dMock = new List<Driver>() {
                new Driver() {
                    DriverID = Guid.NewGuid(),
                    Name = "Frank",
                    Car = "KIA"
                },
                new Driver() {
                    DriverID = Guid.NewGuid(),
                    Name = "Grace",
                    Car = "Skoda"
                },
                new Driver() {
                    DriverID = Guid.NewGuid(),
                    Name = "Heidi",
                    Car = "Renault"
                },
                new Driver() {
                    DriverID = Guid.NewGuid(),
                    Name = "Ivan",
                    Car = "Daewoo"
                },
                new Driver() {
                    DriverID = Guid.NewGuid(),
                    Name = "Judy",
                    Car = "Infinity"
                }
            };
            List<Ride> rMock = new List<Ride> {
                new Ride {
                    RideID              = Guid.NewGuid(),
                    CustomerID          = cMock[0].CustomerID,
                    DriverID            = dMock[0].DriverID,
                    StartAddress        = "Address A",
                    DestinationAddress  = "Address B",
                    Status              = RideStatus.Finished
                },
                new Ride {
                    RideID              = Guid.NewGuid(),
                    CustomerID          = cMock[1].CustomerID,
                    DriverID            = dMock[1].DriverID,
                    StartAddress        = "Address B",
                    DestinationAddress  = "Address C",
                    Status              = RideStatus.Finished
                },
                new Ride {
                    RideID              = Guid.NewGuid(),
                    CustomerID          = cMock[2].CustomerID,
                    DriverID            = dMock[2].DriverID,
                    StartAddress        = "Address C",
                    DestinationAddress  = "Address D",
                    Status              = RideStatus.Finished
                },
                new Ride {
                    RideID              = Guid.NewGuid(),
                    CustomerID          = cMock[3].CustomerID,
                    DriverID            = dMock[3].DriverID,
                    StartAddress        = "Address D",
                    DestinationAddress  = "Address E",
                    Status              = RideStatus.Finished
                },
                new Ride {
                    RideID              = Guid.NewGuid(),
                    CustomerID          = cMock[4].CustomerID,
                    DriverID            = dMock[4].DriverID,
                    StartAddress        = "Address E",
                    DestinationAddress  = "Address F",
                    Status              = RideStatus.Finished
                }
            };

            DataAccessContext dac = DataAccessContext.Instance;
            foreach (Customer customer in cMock)
                dac.Customers.Save(customer);
            foreach (Driver driver in dMock)
                dac.Drivers.Save(driver);
            foreach (Ride ride in rMock)
                dac.Rides.Save(ride);

            dac.Save();
        }
    }
}
