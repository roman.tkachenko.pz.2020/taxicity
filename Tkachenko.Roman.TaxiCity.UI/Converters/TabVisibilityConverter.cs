﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows;

namespace Tkachenko.Roman.TaxiCity.UI.Converters {
    public class TabVisibilityConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            return (string)value == (string)parameter ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
