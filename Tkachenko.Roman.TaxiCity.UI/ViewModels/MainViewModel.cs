﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using Tkachenko.Roman.TaxiCity.UI.Services;
using Tkachenko.Roman.TaxiCity.UI.Utils;
using System.Linq;
using System.Windows.Controls;

namespace Tkachenko.Roman.TaxiCity.UI.ViewModels {
    public class MainViewModel : ViewModelBase {
        public MainViewModel() {
            Update();
        }

        // update all data
        public void Update() {
            DataServiceContext dac = DataServiceContext.Instance;
            Rides = new ObservableCollection<RideViewModel>(dac.Rides.GetAll());
            Customers = new ObservableCollection<CustomerViewModel>(dac.Customers.GetAll());
            Drivers = new ObservableCollection<DriverViewModel>(dac.Drivers.GetAll());
            RequestedRides = new ObservableCollection<RideViewModel>(
                Rides.Where((RideViewModel rvm) => rvm.Status == Model.RideStatus.Requested)
            );
        }

        // update selected customer
        public void UpdateSelectedCustomer() {
            if (SelectedCustomer == null) {
                SelectedCustomerRides = null;
                return;
            }
            Rides = new ObservableCollection<RideViewModel>(DataServiceContext.Instance.Rides.GetAll());
            RequestedRides = new ObservableCollection<RideViewModel>(
                Rides.Where((RideViewModel rvm) => rvm.Status == Model.RideStatus.Requested)
            );
            SelectedCustomerRides = new ObservableCollection<RideViewModel>(
                Rides.Where((RideViewModel rvm) => rvm.CustomerID == SelectedCustomer.CustomerID)
            );
        }

        // update selected driver
        public void UpdateSelectedDriver() {
            if (SelectedDriver == null) {
                SelectedDriverRides = null;
                return;
            }
            Rides = new ObservableCollection<RideViewModel>(DataServiceContext.Instance.Rides.GetAll());
            RequestedRides = new ObservableCollection<RideViewModel>(
                Rides.Where((RideViewModel rvm) => rvm.Status == Model.RideStatus.Requested)
            );
            SelectedDriverRides = new ObservableCollection<RideViewModel>(
                Rides.Where((RideViewModel rvm) => rvm.DriverID == SelectedDriver.DriverID)
            );
        }

        // user control visibility
        private string _visibleTab = "Customers";
        public string VisibleTab { 
            get => _visibleTab;
            set { _visibleTab = value; OnPropertyChanged("VisibleTab"); } 
        }
        public ICommand SetVisibleTab => new Command((object visibility) => VisibleTab = visibility.ToString());

        // customers
        private ObservableCollection<CustomerViewModel> _customers;
        public ObservableCollection<CustomerViewModel> Customers {
            get => _customers;
            set { _customers = value; OnPropertyChanged("Customers"); }
        }

        // drivers
        private ObservableCollection<DriverViewModel> _drivers;
        public ObservableCollection<DriverViewModel> Drivers {
            get => _drivers;
            set { _drivers = value; OnPropertyChanged("Customers"); }
        }

        // rides history
        private ObservableCollection<RideViewModel> _rides;
        public ObservableCollection<RideViewModel> Rides {
            get => _rides;
            set { _rides = value; OnPropertyChanged("Rides"); }
        }

        // user's rides history
        private CustomerViewModel _sCustomer;
        public CustomerViewModel SelectedCustomer {
            get => _sCustomer;
            set { _sCustomer = value; OnPropertyChanged("SelectedCustomer"); UpdateSelectedCustomer(); }
        }

        // user's rides history
        private ObservableCollection<RideViewModel> _scRides;
        public ObservableCollection<RideViewModel> SelectedCustomerRides {
            get => _scRides;
            set { _scRides = value; OnPropertyChanged("SelectedCustomerRides"); }
        }

        // user's ride request
        private string _requestFrom;
        private string _requestTo;
        public string RequestFrom {
            get => _requestFrom;
            set { _requestFrom = value; OnPropertyChanged("RequestFrom"); }
        }
        public string RequestTo {
            get => _requestTo;
            set { _requestTo = value; OnPropertyChanged("RequestTo"); }
        }
        public ICommand RequestRide => new Command((object args) => {
            if (SelectedCustomer == null ||
                RequestFrom == string.Empty ||
                RequestTo == string.Empty) return;
            RideViewModel requestedRide = new RideViewModel() {
                RideID = System.Guid.NewGuid(),
                CustomerID = SelectedCustomer.CustomerID,
                Start = RequestFrom,
                Destination = RequestTo,
                Status = Model.RideStatus.Requested
            };
            DataServiceContext.Instance.Rides.Save(requestedRide);
            RequestFrom = string.Empty;
            RequestTo = string.Empty;
            UpdateSelectedCustomer();
        });

        // user's rides history
        private DriverViewModel _sDriver;
        public DriverViewModel SelectedDriver {
            get => _sDriver;
            set { _sDriver = value; OnPropertyChanged("SelectedDriver"); UpdateSelectedDriver(); }
        }

        // driver's rides history
        private ObservableCollection<RideViewModel> _reqRides;
        public ObservableCollection<RideViewModel> RequestedRides {
            get => _reqRides;
            set { _reqRides = value; OnPropertyChanged("RequestedRides"); }
        }

        // driver's rides history
        private ObservableCollection<RideViewModel> _sdRides;
        public ObservableCollection<RideViewModel> SelectedDriverRides {
            get => _sdRides;
            set { _sdRides = value; OnPropertyChanged("SelectedDriverRides"); }
        }

        // driver list visibility
        private string _visibleDriverTab = "Requests";
        public string VisibleDriverTab {
            get => _visibleDriverTab;
            set { _visibleDriverTab = value; OnPropertyChanged("VisibleDriverTab"); }
        }
        public ICommand SetVisibleDriverTab => new Command((object visibility) => VisibleDriverTab = visibility.ToString());

        // driver assign command
        private RideViewModel _sRequest;
        public RideViewModel SelectedRequest {
            get => _sRequest;
            set { _sRequest = value; OnPropertyChanged("SelectedRequest"); }
        }
        public ICommand AssignToDriver => new Command(
            (object args) => {
                if (SelectedDriver == null || SelectedRequest == null) return;
                SelectedRequest.DriverID = SelectedDriver.DriverID;
                SelectedRequest.Status = Model.RideStatus.InProgress;
                DataServiceContext.Instance.Rides.Update(SelectedRequest.RideID, SelectedRequest);
                UpdateSelectedDriver();
            }
        );

        // driver finish command
        private RideViewModel _sProgressRide;
        public RideViewModel SelectedProgressRide {
            get => _sProgressRide;
            set { _sProgressRide = value; OnPropertyChanged("SelectedProgressRide"); }
        }
        public ICommand FinishRide => new Command(
            (object args) => {
                if (SelectedDriver == null || SelectedProgressRide == null || 
                    SelectedProgressRide.Status != Model.RideStatus.InProgress) return;
                SelectedProgressRide.Status = Model.RideStatus.Finished;
                DataServiceContext.Instance.Rides.Update(SelectedProgressRide.RideID, SelectedProgressRide);
                UpdateSelectedDriver();
                UpdateSelectedCustomer();
            }
        );
    }
}
