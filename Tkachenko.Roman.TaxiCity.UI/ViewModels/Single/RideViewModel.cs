﻿using System;
using Tkachenko.Roman.TaxiCity.Model;

namespace Tkachenko.Roman.TaxiCity.UI.ViewModels {
    public class RideViewModel : ViewModelBase {
        private Guid _rideID;
        private Guid _customerID;
        private string _customerName;
        private Guid _driverID;
        private string _driverName;
        private string _startAddress;
        private string _destinationAddress;
        private RideStatus _status;

        public Guid RideID {
            get => _rideID;
            set { _rideID = value; OnPropertyChanged("ID"); }
        }
        public Guid CustomerID {
            get => _customerID;
            set { _customerID = value; OnPropertyChanged("CustomerID"); }
        }
        public string Customer {
            get => _customerName;
            set { _customerName = value; OnPropertyChanged("CustomerName"); }
        }
        public Guid DriverID {
            get => _driverID;
            set { _driverID = value; OnPropertyChanged("DriverID"); }
        }
        public string Driver {
            get => _driverName;
            set { _driverName = value; OnPropertyChanged("DriverName"); }
        }
        public string Start { 
            get => _startAddress;
            set { _startAddress = value; OnPropertyChanged("Start"); }
        }
        public string Destination { 
            get => _destinationAddress;
            set { _destinationAddress = value; OnPropertyChanged("Destination"); }
        }
        public RideStatus Status { 
            get => _status;
            set { _status = value; OnPropertyChanged("Status"); } 
        }
    }
}
