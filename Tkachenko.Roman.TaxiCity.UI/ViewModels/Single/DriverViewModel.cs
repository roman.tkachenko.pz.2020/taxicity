﻿using System;

namespace Tkachenko.Roman.TaxiCity.UI.ViewModels {
    public class DriverViewModel : ViewModelBase {
        private Guid _driverID;
        private string _name;
        private string _car;

        public Guid DriverID {
            get => _driverID;
            set { _driverID = value; OnPropertyChanged("ID"); } 
        }
        public string Name { 
            get => _name;
            set { _name = value; OnPropertyChanged("Name"); } 
        }
        public string Car {
            get => _car;
            set { _car = value; OnPropertyChanged("Car"); }
        }
    }
}
