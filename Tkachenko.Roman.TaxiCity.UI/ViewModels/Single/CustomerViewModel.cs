﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tkachenko.Roman.TaxiCity.UI.ViewModels {
    public class CustomerViewModel : ViewModelBase{
        private Guid _customerID;
        private string _name;

        public Guid CustomerID { 
            get => _customerID; 
            set { _customerID = value; OnPropertyChanged("ID"); } 
        }
        public string Name { 
            get => _name; 
            set { _name = value; OnPropertyChanged("Name"); }
        }
    }
}
