﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tkachenko.Roman.TaxiCity.UI.Services;
using Tkachenko.Roman.TaxiCity.UI.ViewModels;

namespace Tkachenko.Roman.TaxiCity.UI.Views {
    /// <summary>
    /// Interaction logic for DriversUserControl.xaml
    /// </summary>
    public partial class DriversUserControl : UserControl {
        public DriversUserControl() {
            InitializeComponent();
        }

        private void CheckRemovedDriver(object sender, KeyEventArgs e) {
            if (Key.Delete == e.Key) {
                DriverViewModel driver = (sender as DataGrid).SelectedItem as DriverViewModel;
                if (driver != null) {
                    DataServiceContext.Instance.Drivers.Remove(driver.DriverID);
                    (DataContext as MainViewModel).Update();
                }
            }
        }
        private void CreateDriver(object sender, AddingNewItemEventArgs e) {
            e.NewItem = new DriverViewModel {
                DriverID = Guid.NewGuid(),
                Name = "Driver",
                Car = "Car"
            };
            DataServiceContext.Instance.Drivers.Save(e.NewItem as DriverViewModel);
        }
        private void ChangeDriver(object sender, DataGridCellEditEndingEventArgs e) {
            DriverViewModel driver = e.Row.Item as DriverViewModel;
            if (e.Column.Header.ToString() == "Name") driver.Name = ((TextBox)e.EditingElement).Text;
            else driver.Car = ((TextBox)e.EditingElement).Text;
            DataServiceContext.Instance.Drivers.Update(driver.DriverID, driver);
        }
    }
}
