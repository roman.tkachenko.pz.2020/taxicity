﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tkachenko.Roman.TaxiCity.UI.Services;
using Tkachenko.Roman.TaxiCity.UI.ViewModels;

namespace Tkachenko.Roman.TaxiCity.UI.Views {
    /// <summary>
    /// Interaction logic for CustomersUserControl.xaml
    /// </summary>
    public partial class CustomersUserControl : UserControl {
        public CustomersUserControl() {
            InitializeComponent();
        }

        private void CheckRemovedCustomer(object sender, KeyEventArgs e) {
            if (Key.Delete == e.Key) {
                CustomerViewModel customer = (sender as DataGrid).SelectedItem as CustomerViewModel;
                if (customer != null) {
                    DataServiceContext.Instance.Customers.Remove(customer.CustomerID);
                    (DataContext as MainViewModel).Update();
                }
            }
        }
        private void CreateCustomer(object sender, AddingNewItemEventArgs e) {
            e.NewItem = new CustomerViewModel {
                CustomerID = Guid.NewGuid(),
                Name = "Customer"
            };
            DataServiceContext.Instance.Customers.Save(e.NewItem as CustomerViewModel);
        }
        private void ChangeCustomer(object sender, DataGridCellEditEndingEventArgs e) {
            CustomerViewModel customer = e.Row.Item as CustomerViewModel;
            customer.Name = ((TextBox)e.EditingElement).Text;
            DataServiceContext.Instance.Customers.Update(customer.CustomerID, customer);
        }

        private void CheckRemovedRide(object sender, KeyEventArgs e) {
            if (Key.Delete == e.Key) {
                RideViewModel ride = (sender as DataGrid).SelectedItem as RideViewModel;
                if (ride != null) {
                    DataServiceContext.Instance.Rides.Remove(ride.RideID);
                    (DataContext as MainViewModel).UpdateSelectedCustomer();
                }
            }
        }
    }
}
