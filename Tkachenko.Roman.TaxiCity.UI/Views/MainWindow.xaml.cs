﻿using System.Collections;
using System.Collections.Generic;
using System.Windows;
using Tkachenko.Roman.TaxiCity.UI.Services;
using Tkachenko.Roman.TaxiCity.UI.ViewModels;

namespace Tkachenko.Roman.TaxiCity.UI {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }
    }
}
