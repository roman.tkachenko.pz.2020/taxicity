﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using Tkachenko.Roman.TaxiCity.API.Services;
using Tkachenko.Roman.TaxiCity.API.ViewModels;
using Tkachenko.Roman.TaxiCity.Model;

namespace Tkachenko.Roman.TaxiCity.API.Controllers {
    [EnableCors("Cors")]
    [Route("api/ride")]
    public class RideController : Controller {
        private readonly IService<Ride, RideViewModel> _rideService;

        public RideController(IService<Ride, RideViewModel> rideService) {
            _rideService = rideService;
        }

        [HttpGet]
        public ActionResult Index() => Ok(_rideService.GetAll());

        [HttpGet]
        [Route("{id}")]
        public ActionResult Details(Guid id) => Ok(_rideService.Get(id));

        [HttpPost]
        [Route("new")]
        public ActionResult Create([FromBody] RideViewModel ride) {
            ride.RideID = Guid.NewGuid();
            _rideService.Save(ride);
            return Ok();
        }

        [HttpPost]
        [Route("update/{id}")]
        public ActionResult Edit(Guid id, [FromBody] RideViewModel ride) {
            _rideService.Update(id, ride);
            return Ok();
        }

        [HttpGet]
        [Route("delete/{id}")]
        public ActionResult Delete(Guid id) {
            _rideService.Remove(id);
            return Ok();
        }
    }
}
