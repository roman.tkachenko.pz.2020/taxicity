﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using Tkachenko.Roman.TaxiCity.API.Services;
using Tkachenko.Roman.TaxiCity.API.ViewModels;
using Tkachenko.Roman.TaxiCity.Model;

namespace Tkachenko.Roman.TaxiCity.API.Controllers {
    [EnableCors("Cors")]
    [Route("api/driver")]
    public class DriverController : Controller {
        private readonly IService<Driver, DriverViewModel> _driverService;

        public DriverController(IService<Driver, DriverViewModel> driverService) {
            _driverService = driverService;
        }

        [HttpGet]
        public ActionResult Index() => Ok(_driverService.GetAll());

        [HttpGet]
        [Route("{id}")]
        public ActionResult Details(Guid id) => Ok(_driverService.Get(id));

        [HttpPost]
        [Route("new")]
        public ActionResult Create([FromBody] DriverViewModel driver) {
            driver.DriverID = Guid.NewGuid();
            _driverService.Save(driver);
            return Ok();
        }

        [HttpPost]
        [Route("update/{id}")]
        public ActionResult Edit(Guid id, [FromBody] DriverViewModel driver) {
            _driverService.Update(id, driver);
            return Ok();
        }
        
        [HttpGet]
        [Route("delete/{id}")]
        public ActionResult Delete(Guid id) {
            _driverService.Remove(id);
            return Ok();
        }
    }
}
