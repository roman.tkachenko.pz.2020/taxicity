﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using Tkachenko.Roman.TaxiCity.API.Services;
using Tkachenko.Roman.TaxiCity.API.ViewModels;
using Tkachenko.Roman.TaxiCity.Model;

namespace Tkachenko.Roman.TaxiCity.API.Controllers {
    [EnableCors("Cors")]
    [Route("api/customer")]
    public class CustomerController : Controller {
        private readonly IService<Customer, CustomerViewModel> _customerService; 

        public CustomerController(IService<Customer, CustomerViewModel> customerService) {
            _customerService = customerService;
        }

        [HttpGet]
        public ActionResult Index() => Ok(_customerService.GetAll());

        [HttpGet]
        [Route("{id}")]
        public ActionResult Details(Guid id) => Ok(_customerService.Get(id));

        [HttpPost]
        [Route("new")]
        public ActionResult Create([FromBody] CustomerViewModel customer) {
            customer.CustomerID = Guid.NewGuid();
            _customerService.Save(customer);
            return Ok();
        }

        [HttpPost]
        [Route("update/{id}")]
        public ActionResult Edit(Guid id, [FromBody] CustomerViewModel customer) {
            _customerService.Update(id, customer);
            return Ok();
        }
        
        [HttpGet]
        [Route("delete/{id}")]
        public ActionResult Delete(Guid id) {
            _customerService.Remove(id);
            return Ok();
        }
    }
}
