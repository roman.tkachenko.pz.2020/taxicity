﻿using System;
using System.Collections.Generic;
using Tkachenko.Roman.TaxiCity.API.ViewModels;

namespace Tkachenko.Roman.TaxiCity.API.Services {
    public interface IService<M, VM> where VM : ViewModelBase {
        // mappers
        public M MapToModel(VM viewModel);
        public IEnumerable<M> MapToModels(IEnumerable<VM> viewModels);
        public VM MapToViewModel(M model);
        public IEnumerable<VM> MapToViewModels(IEnumerable<M> models);

        // wrappers for repository
        public IEnumerable<VM> GetAll();
        public VM Get(Guid id);
        public void Save(VM viewModel);
        public void Update(Guid id, VM viewModel);
        public void Remove(Guid id);
    }
}
