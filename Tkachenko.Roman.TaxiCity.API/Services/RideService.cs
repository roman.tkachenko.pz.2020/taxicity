﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tkachenko.Roman.TaxiCity.Model;
using Tkachenko.Roman.TaxiCity.Model.DataAccess;
using Tkachenko.Roman.TaxiCity.API.ViewModels;

namespace Tkachenko.Roman.TaxiCity.API.Services {
    public class RideService : IService<Ride, RideViewModel> {
        private readonly IRepository<Ride> _rideRepository;
        private readonly IRepository<Driver> _driverRepository;
        private readonly IRepository<Customer> _customerRepository;

        public RideService() {
            DataAccessContext context = DataAccessContext.Instance;
            _rideRepository = context.Rides;
            _driverRepository = context.Drivers;
            _customerRepository = context.Customers;
        }

        public Ride MapToModel(RideViewModel viewModel) {
            string driverName, customerName;
            Guid driverID, customerID;

            // find customer
            if (viewModel.CustomerID != Guid.Empty) {
                customerID = viewModel.CustomerID;
                customerName = _customerRepository.Get(viewModel.CustomerID).Name;
            } else {
                if (viewModel.Customer == null) throw new Exception("Please, specify the customer");
                customerName = viewModel.Customer;
                try {
                    customerID = _customerRepository.GetAll()
                        .Where((Customer customer) => customer.Name == viewModel.Customer)
                        .FirstOrDefault()
                        .CustomerID;
                } catch { throw new Exception("Unable to find specified customer"); }
            }

            // find driver
            if (viewModel.Status == RideStatus.Requested) {
                driverID = Guid.Empty;
                driverName = null;
            } else {
                if (viewModel.DriverID != Guid.Empty) {
                    driverID = viewModel.DriverID;
                    driverName = _driverRepository.Get(viewModel.DriverID).Name;
                } else {
                    if (viewModel.Driver == null) throw new Exception("Please, specify driver");
                    driverName = viewModel.Driver;
                    try {
                        driverID = _driverRepository.GetAll()
                            .Where((Driver driver) => driver.Name == viewModel.Driver)
                            .SingleOrDefault()
                            .DriverID;
                    } catch { throw new Exception("Unable to find specified driver"); }
                }
            }

            return new Ride() {
                RideID = viewModel.RideID,
                DriverID = driverID,
                CustomerID = customerID,
                StartAddress = viewModel.Start,
                DestinationAddress = viewModel.Destination,
                Status = viewModel.Status
            };
        }
        public IEnumerable<Ride> MapToModels(IEnumerable<RideViewModel> viewModels) =>
            viewModels.Select((RideViewModel vm) => MapToModel(vm));

        public RideViewModel MapToViewModel(Ride model) {
            string driverName, customerName;

            // find customer
            if (model.CustomerID == Guid.Empty) throw new Exception("Please, specify the customer");
            try {
                Customer customer = _customerRepository.GetAll()
                    .Where((Customer customer) => customer.CustomerID == model.CustomerID)
                    .SingleOrDefault();
                if (customer == null) return null;
                customerName = customer.Name;
            } catch (NullReferenceException) { throw new Exception("Unable to find specified customer"); }

            // find driver
            if (model.Status == RideStatus.Requested) {
                driverName = null;
            } else {
                if (model.DriverID == Guid.Empty) throw new Exception("Please, specify driver");
                try {
                    Driver driver = _driverRepository.GetAll()
                        .Where((Driver driver) => driver.DriverID == model.DriverID)
                        .FirstOrDefault();
                    if (driver == null) return null;
                    driverName = driver.Name;
                }
                catch { throw new Exception("Unable to find specified driver"); }
            }

            return new RideViewModel() {
                RideID = model.RideID,
                DriverID = model.DriverID,
                Driver = driverName,
                CustomerID = model.CustomerID,
                Customer = customerName,
                Start = model.StartAddress,
                Destination = model.DestinationAddress,
                Status = model.Status
            };
        }
        public IEnumerable<RideViewModel> MapToViewModels(IEnumerable<Ride> models) =>
            models.Select((Ride m) => MapToViewModel(m)).Where((RideViewModel rvm) => rvm != null);

        public RideViewModel Get(Guid id) => MapToViewModel(_rideRepository.Get(id));
        public IEnumerable<RideViewModel> GetAll() => MapToViewModels(_rideRepository.GetAll());
        public void Save(RideViewModel viewModel) => _rideRepository.Save(MapToModel(viewModel));
        public void Remove(Guid id) => _rideRepository.Remove(id);
        public void Update(Guid id, RideViewModel viewModel) => _rideRepository.Update(id, MapToModel(viewModel));
    }
}