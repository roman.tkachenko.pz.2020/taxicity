﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tkachenko.Roman.TaxiCity.Model;
using Tkachenko.Roman.TaxiCity.Model.DataAccess;
using Tkachenko.Roman.TaxiCity.API.ViewModels;

namespace Tkachenko.Roman.TaxiCity.API.Services {
    public class DriverService : IService<Driver, DriverViewModel> {
        private readonly IRepository<Driver> _repository;

        public DriverService() {
            _repository = DataAccessContext.Instance.Drivers;
        }

        public Driver MapToModel(DriverViewModel viewModel) =>
            new Driver() {
                DriverID = viewModel.DriverID,
                Name = viewModel.Name,
                Car = viewModel.Car
            };
        public IEnumerable<Driver> MapToModels(IEnumerable<DriverViewModel> viewModels) =>
            viewModels.Select((DriverViewModel vm) => MapToModel(vm));

        public DriverViewModel MapToViewModel(Driver model) =>
            new DriverViewModel() {
                DriverID = model.DriverID,
                Name = model.Name,
                Car = model.Car
            };
        public IEnumerable<DriverViewModel> MapToViewModels(IEnumerable<Driver> models) =>
            models.Select((Driver m) => MapToViewModel(m));

        public DriverViewModel Get(Guid id) => MapToViewModel(_repository.Get(id));
        public IEnumerable<DriverViewModel> GetAll() => MapToViewModels(_repository.GetAll());
        public void Save(DriverViewModel viewModel) => _repository.Save(MapToModel(viewModel));
        public void Remove(Guid id) => _repository.Remove(id);
        public void Update(Guid id, DriverViewModel viewModel) => _repository.Update(id, MapToModel(viewModel));
    }
}
