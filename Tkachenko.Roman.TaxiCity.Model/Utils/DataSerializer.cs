﻿using System.IO;
using System.Runtime.Serialization;

namespace Tkachenko.Roman.TaxiCity.Model
{
    public static class DataSerializer
    {
        public static T ReadData<T>(string dataPath)
        {
            FileStream stream = new FileStream(dataPath, FileMode.Open);
            DataContractSerializer serializer = new DataContractSerializer(typeof(T));
            T data = (T)serializer.ReadObject(stream);
            stream.Close();
            return data;
        }

        public static void SaveData<T>(string dataPath, T data)
        {
            DataContractSerializer serializer = new DataContractSerializer(typeof(T));
            FileStream stream = new FileStream(dataPath, FileMode.Create);
            serializer.WriteObject(stream, data);
            stream.Close();
        }
    }
}
