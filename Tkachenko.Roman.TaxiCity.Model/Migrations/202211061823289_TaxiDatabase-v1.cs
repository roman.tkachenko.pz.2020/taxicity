﻿namespace Tkachenko.Roman.TaxiCity.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TaxiDatabasev1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        CustomerID = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.CustomerID);
            
            CreateTable(
                "dbo.Drivers",
                c => new
                    {
                        DriverID = c.Guid(nullable: false),
                        Name = c.String(),
                        Car = c.String(),
                    })
                .PrimaryKey(t => t.DriverID);
            
            CreateTable(
                "dbo.Rides",
                c => new
                    {
                        RideID = c.Guid(nullable: false),
                        CustomerID = c.Guid(nullable: false),
                        DriverID = c.Guid(nullable: false),
                        StartAddress = c.String(),
                        DestinationAddress = c.String(),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RideID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Rides");
            DropTable("dbo.Drivers");
            DropTable("dbo.Customers");
        }
    }
}
