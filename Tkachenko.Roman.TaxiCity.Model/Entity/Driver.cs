﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Tkachenko.Roman.TaxiCity.Model
{
    [DataContract]
    public class Driver
    {
        [DataMember]
        public Guid DriverID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Car { get; set; }
    }
}
