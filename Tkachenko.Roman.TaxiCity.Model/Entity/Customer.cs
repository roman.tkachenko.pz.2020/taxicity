﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Tkachenko.Roman.TaxiCity.Model
{
    [DataContract]
    public class Customer
    {
        [DataMember]
        public Guid CustomerID { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
