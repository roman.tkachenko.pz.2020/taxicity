﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Tkachenko.Roman.TaxiCity.Model
{
    [DataContract]
    public class Ride
    {
        [DataMember]
        public Guid RideID { get; set; }

        [DataMember]
        public Guid CustomerID { get; set; }

        [DataMember]
        public Guid DriverID { get; set; }

        [DataMember]
        public string StartAddress { get; set; }

        [DataMember]
        public string DestinationAddress { get; set; }

        [DataMember]
        public RideStatus Status { get; set; }
    }

    [DataContract]
    public enum RideStatus {
        [EnumMember]
        Requested,
        [EnumMember]
        InProgress,
        [EnumMember]
        Finished
    }
}
