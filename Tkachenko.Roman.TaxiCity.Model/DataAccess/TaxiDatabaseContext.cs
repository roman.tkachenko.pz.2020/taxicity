﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Text;

namespace Tkachenko.Roman.TaxiCity.Model.DataAccess {
    public class TaxiDatabaseContext : DbContext {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<Ride> Rides { get; set; }

        /**
         * DB-First approach
         */
        public TaxiDatabaseContext(string connectionString) : base(connectionString) {
            Database.SetInitializer<TaxiDatabaseContext>(null);
        }

        /**
         * Code-First approach
         */
        //public TaxiDatabaseContext() : base("data source=WINDOWS-C7QFA7J;initial catalog=Tkachenko.Roman.TaxiCity;trusted_connection=true") {
        //    Database.SetInitializer(new MigrateDatabaseToLatestVersion<TaxiDatabaseContext, Migrations.Configuration>());
        //}

        //protected override void OnModelCreating(DbModelBuilder modelBuilder) { }
    }
}
