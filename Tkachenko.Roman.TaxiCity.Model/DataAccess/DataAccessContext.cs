﻿using System.Data.Entity;
using System.IO;

namespace Tkachenko.Roman.TaxiCity.Model.DataAccess {
    internal enum StorageType {
        Files,
        Database
    }
    public class DataAccessContext {
        // current storage type
        internal static readonly StorageType STORAGE = StorageType.Database;

        // paths to data storage and database
        public static readonly string STORAGE_PATH = "tc_data";
        public static readonly string CONNECTION_STRING = "data source=WINDOWS-C7QFA7J;initial catalog=Tkachenko.Roman.TaxiCity;trusted_connection=true";

        // data access repositories
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Driver> _driverRepository;
        private readonly IRepository<Ride> _rideRepository;

        // database context
        private readonly DbContext _dbContext;

        // singleton implementation
        private static DataAccessContext _instance;
        public static DataAccessContext Instance {
            get {
                if (_instance == null) {
                    _instance = new DataAccessContext();
                }
                return _instance;
            }
        }

        // access to repositories
        public IRepository<Customer> Customers => _customerRepository;
        public IRepository<Driver> Drivers => _driverRepository;
        public IRepository<Ride> Rides => _rideRepository;

        private DataAccessContext() {
            // create storage directory
            Directory.CreateDirectory(STORAGE_PATH);

            // initialize repositories in dependence on storage type
            switch (STORAGE) {
                case StorageType.Files:
                    _customerRepository = new SerializationRepositories.CustomerRepository();
                    _driverRepository = new SerializationRepositories.DriverRepository();
                    _rideRepository = new SerializationRepositories.RideRepository();
                    break;
                case StorageType.Database:
                    TaxiDatabaseContext tdc = new TaxiDatabaseContext(CONNECTION_STRING);
                    _dbContext = tdc;
                    _customerRepository = new DatabaseRepositories.CustomerRepository(CONNECTION_STRING, tdc.Customers, tdc);
                    _driverRepository = new DatabaseRepositories.DriverRepository(CONNECTION_STRING, tdc.Drivers, tdc);
                    _rideRepository = new DatabaseRepositories.RideRepository(CONNECTION_STRING, tdc.Rides, tdc);
                    break;
            }
        }

        // write all data back
        public void Save() {
            switch (STORAGE) {
                case StorageType.Files:
                    _customerRepository.WriteData();
                    _driverRepository.WriteData();
                    _rideRepository.WriteData();
                    break;
                case StorageType.Database:
                    _dbContext.SaveChanges();
                    break;
            }
        }
    }
}
