﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace Tkachenko.Roman.TaxiCity.Model.DataAccess.DatabaseRepositories {
    public class CustomerRepository : IRepository<Customer> {
        public string DataPath { get; private set; }

        private DbContext contextRef;
        private DbSet<Customer> data;

        public CustomerRepository(string connectionString, DbSet<Customer> set, DbContext context) {
            DataPath = connectionString;
            data = set;
            contextRef = context;
        }

        public Customer Get(Guid id) =>
            data.Where((Customer customer) => customer.CustomerID == id)
                .SingleOrDefault();

        public IEnumerable<Customer> GetAll() => data.ToList();

        public void Remove(Guid id) {
            Customer customer = Get(id);
            if (customer != null) {
                data.Remove(customer);
            }
            contextRef.SaveChanges();
        }

        public void Save(Customer entity) {
            data.Add(entity);
            contextRef.SaveChanges();
        }

        public void Update(Guid id, Customer entity) {
            string updateQuery = @"
                UPDATE [dbo].[Customers]
                SET CustomerID = @customerID,
                    Name = @name
                WHERE CustomerID = @id;
            ";
            using (SqlConnection con = new SqlConnection(DataPath)) {
                SqlCommand com = new SqlCommand(updateQuery, con);
                com.Parameters.AddWithValue("@customerID", entity.CustomerID);
                com.Parameters.AddWithValue("@name", entity.Name);
                com.Parameters.AddWithValue("@id", id);
                con.Open();
                com.ExecuteNonQuery();
                con.Close();
            }
            contextRef.Entry(Get(id)).Reload();
            contextRef.SaveChanges();
        }

        public void ReadData() {}
        public void WriteData() {}
    }
}
