﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace Tkachenko.Roman.TaxiCity.Model.DataAccess.DatabaseRepositories {
    public class RideRepository : IRepository<Ride> {
        public string DataPath { get; private set; }

        private DbContext contextRef;
        private DbSet<Ride> data;

        public RideRepository(string connectionString, DbSet<Ride> set, DbContext context) {
            DataPath = connectionString;
            data = set;
            contextRef = context;
        }

        public Ride Get(Guid id) =>
            data.Where((Ride ride) => ride.RideID == id)
                .SingleOrDefault();

        public IEnumerable<Ride> GetAll() => data.ToList();

        public void Remove(Guid id) {
            Ride ride = Get(id);
            if (ride != null) {
                data.Remove(ride);
            }
            contextRef.SaveChanges();
        }

        public void Save(Ride entity) {
            data.Add(entity);
            contextRef.SaveChanges();
        }

        public void Update(Guid id, Ride entity) {
            Ride current = Get(id);
            current.RideID = entity.RideID;
            current.CustomerID = entity.CustomerID;
            current.DriverID = entity.DriverID;
            current.StartAddress = entity.StartAddress;
            current.DestinationAddress = entity.DestinationAddress;
            current.Status = entity.Status;
            string updateQuery = @"
                UPDATE [dbo].[Rides]
                SET RideID = @rideID,
                    CustomerID = @customerID,
                    DriverID = @driverID,
                    StartAddress = @startAddress,
                    DestinationAddress = @destinationAddress,
                    Status = @status
                WHERE RideID = @id;
            ";
            using (SqlConnection con = new SqlConnection(DataPath)) {
                SqlCommand com = new SqlCommand(updateQuery, con);
                com.Parameters.AddWithValue("@rideID", entity.RideID);
                com.Parameters.AddWithValue("@customerID", entity.CustomerID);
                com.Parameters.AddWithValue("@driverID", entity.DriverID);
                com.Parameters.AddWithValue("@startAddress", entity.StartAddress);
                com.Parameters.AddWithValue("@destinationAddress", entity.DestinationAddress);
                com.Parameters.AddWithValue("@status", entity.Status);
                com.Parameters.AddWithValue("@id", id);
                con.Open();
                com.ExecuteNonQuery();
                con.Close();
            }
            contextRef.Entry(Get(id)).Reload();
            contextRef.SaveChanges();
        }

        public void ReadData() {}
        public void WriteData() {}
    }
}
