﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace Tkachenko.Roman.TaxiCity.Model.DataAccess.DatabaseRepositories {
    public class DriverRepository : IRepository<Driver> {
        public string DataPath { get; private set; }

        private DbContext contextRef;
        private DbSet<Driver> data;

        public DriverRepository(string connectionString, DbSet<Driver> set, DbContext context) {
            DataPath = connectionString;
            data = set;
            contextRef = context;
        }

        public Driver Get(Guid id) =>
            data.Where((Driver driver) => driver.DriverID == id)
                .SingleOrDefault();

        public IEnumerable<Driver> GetAll() => data.ToList();

        public void Remove(Guid id) {
            Driver driver = Get(id);
            if (driver != null) {
                data.Remove(driver);
            }
            contextRef.SaveChanges();
        }

        public void Save(Driver entity) {
            data.Add(entity);
            contextRef.SaveChanges();
        }

        public void Update(Guid id, Driver entity) {
            string updateQuery = @"
                UPDATE [dbo].[Drivers]
                SET DriverID = @driverID,
                    Name = @name,
                    Car = @car
                WHERE DriverID = @id;
            ";
            using (SqlConnection con = new SqlConnection(DataPath)) {
                SqlCommand com = new SqlCommand(updateQuery, con);
                com.Parameters.AddWithValue("@driverID", entity.DriverID);
                com.Parameters.AddWithValue("@name", entity.Name);
                com.Parameters.AddWithValue("@car", entity.Car);
                com.Parameters.AddWithValue("@id", id);
                con.Open();
                com.ExecuteNonQuery();
                con.Close();
            }
            contextRef.Entry(Get(id)).Reload();
            contextRef.SaveChanges();
        }

        public void ReadData() {}
        public void WriteData() {}
    }
}
