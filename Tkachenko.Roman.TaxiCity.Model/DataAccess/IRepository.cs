﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tkachenko.Roman.TaxiCity.Model.DataAccess {
    public interface IRepository<T> {
        public string DataPath { get; }
        public IEnumerable<T> GetAll();
        public T Get(Guid id);
        public void Save(T entity);
        public void Update(Guid id, T entity);
        public void Remove(Guid id);
        public void ReadData();
        public void WriteData();
    }
}
