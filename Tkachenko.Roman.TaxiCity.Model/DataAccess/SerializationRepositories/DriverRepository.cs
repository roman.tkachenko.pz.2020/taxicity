﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Tkachenko.Roman.TaxiCity.Model.DataAccess.SerializationRepositories {
    public class DriverRepository : IRepository<Driver> {
        public static readonly string DEFAULT_PATH = "tc_data/drivers.dat";
        public string DataPath { get; private set; }

        private List<Driver> data;

        public DriverRepository() {
            DataPath = DEFAULT_PATH;
            ReadData();
        }

        public Driver Get(Guid id) =>
            data.Where((Driver driver) => driver.DriverID == id)
                .SingleOrDefault();

        public IEnumerable<Driver> GetAll() => data;

        public void Remove(Guid id) => data = data.Where((Driver driver) => driver.DriverID != id).ToList();

        public void Save(Driver entity) => data.Add(entity);

        public void Update(Guid id, Driver entity) {
            Driver current = Get(id);
            current.DriverID = entity.DriverID;
            current.Name = entity.Name;
            current.Car = entity.Car;
        }

        public void ReadData() =>
            data = File.Exists(DataPath) ? 
                DataSerializer.ReadData<List<Driver>>(DataPath) : 
                new List<Driver>();

        public void WriteData() => DataSerializer.SaveData(DataPath, data);
    }
}
