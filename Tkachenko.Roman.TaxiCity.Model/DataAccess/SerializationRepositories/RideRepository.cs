﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Tkachenko.Roman.TaxiCity.Model.DataAccess.SerializationRepositories {
    public class RideRepository : IRepository<Ride> {
        public static readonly string DEFAULT_PATH = "tc_data/rides.dat";
        public string DataPath { get; private set; }

        private List<Ride> data;

        public RideRepository() {
            DataPath = DEFAULT_PATH;
            ReadData();
        }

        public Ride Get(Guid id) =>
            data.Where((Ride ride) => ride.RideID == id)
                .SingleOrDefault();

        public IEnumerable<Ride> GetAll() => data;

        public void Remove(Guid id) => data = data.Where((Ride ride) => ride.RideID != id).ToList();

        public void Save(Ride entity) => data.Add(entity);

        public void Update(Guid id, Ride entity) {
            Ride current = Get(id);
            current.RideID = entity.RideID;
            current.CustomerID = entity.CustomerID;
            current.DriverID = entity.DriverID;
            current.StartAddress = entity.StartAddress;
            current.DestinationAddress = entity.DestinationAddress;
            current.Status = entity.Status;
        }

        public void ReadData() =>
            data = File.Exists(DataPath) ?
                DataSerializer.ReadData<List<Ride>>(DataPath) :
                new List<Ride>();

        public void WriteData() => DataSerializer.SaveData(DataPath, data);
    }
}
