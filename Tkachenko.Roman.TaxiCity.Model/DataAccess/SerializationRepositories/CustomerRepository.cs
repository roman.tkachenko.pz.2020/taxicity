﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Tkachenko.Roman.TaxiCity.Model.DataAccess.SerializationRepositories {
    public class CustomerRepository : IRepository<Customer> {
        public static readonly string DEFAULT_PATH = "tc_data/customers.dat";
        public string DataPath { get; private set; }

        private List<Customer> data;

        public CustomerRepository() {
            DataPath = DEFAULT_PATH;
            ReadData();
        }

        public Customer Get(Guid id) =>
            data.Where((Customer customer) => customer.CustomerID == id)
                .SingleOrDefault();

        public IEnumerable<Customer> GetAll() => data;

        public void Remove(Guid id) => data = data.Where((Customer customer) => customer.CustomerID != id).ToList();

        public void Save(Customer entity) => data.Add(entity);

        public void Update(Guid id, Customer entity) {
            Customer current = Get(id);
            current.CustomerID = entity.CustomerID;
            current.Name = entity.Name;
        }

        public void ReadData() =>
            data = File.Exists(DataPath) ?
                DataSerializer.ReadData<List<Customer>>(DataPath) :
                new List<Customer>();

        public void WriteData() => DataSerializer.SaveData(DataPath, data);
    }
}
