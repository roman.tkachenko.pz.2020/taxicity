import React, { Component } from "react";
import DriverList from "../lists/DriverList";

class DriverPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            drivers: []
        }
    }

    getDrivers() {
        fetch("https://localhost:44382/api/driver", {
            method: "GET"
        })
        .then(res => res.json())
        .then((result) => {
            this.setState({
                drivers: result
            });
        });
    }

    componentDidMount() {
        this.getDrivers();
    }

    render() { 
        return <>
            <div className="row mt-0 p-1 border rounded">
                <div className="d-flex flex-row justify-content-between"
                    style={{fontSize: "larger"}}>
                    Header
                </div>
            </div>
            <div className="row mt-2 p-2 border rounded">
                <DriverList drivers={this.state.drivers}/>
            </div>
        </>;
    }
}
 
export default DriverPage;