import React, { Component } from "react";
import { assignRide, finishRide, requestRide } from "../../utils/requests";
import CustomerList from "../lists/CustomerList";
import DriverList from "../lists/DriverList";
import RideList from "../lists/RideList";

class HomePage extends Component {
    constructor(props) {
        super(props);
        this.cRef = React.createRef();
        this.dRef = React.createRef();
        this.rRef = React.createRef();
        this.fRef = React.createRef();
        this.state = {
            from: null,
            to: null,
            requestRide: null,
            progressRide: null,
            customer: null,
            cName: null,
            driver: null,
            customers: [],
            drivers: [],
            rides: []
        }
    }
    
    getRides() {
        fetch("https://localhost:44382/api/ride", {
            method: "GET"
        })
        .then(res => res.json())
        .then((result) => {
            this.setState({
                rides: result
            });
        });
    }

    getDrivers() {
        fetch("https://localhost:44382/api/driver", {
            method: "GET"
        })
        .then(res => res.json())
        .then((result) => {
            this.setState({
                drivers: result
            });
            this.getRides();
        });
    }

    getCustomers() {
        fetch("https://localhost:44382/api/customer", {
            method: "GET"
        })
        .then(res => res.json())
        .then((result) => {
            this.setState({
                customers: result
            });
            this.getDrivers();
        });
    }

    componentDidMount() {
        this.getCustomers();
    }

    render() { 
        let requestActive = this.state.from !== null && this.state.from !== "" &&
                            this.state.to !== null && this.state.to !== "";
        
        let rideRequested = this.state.rides.filter(ride => ride.status === 0).length > 0;
        let availableDrivers = this.state.drivers.length > 0;

        let assignActive = rideRequested && availableDrivers;

        let customerOptions = [];
        for (let customer in this.state.customers) {
            customerOptions.push(<option key={this.state.customers[customer].customerID} value={this.state.customers[customer].customerID}>
                {this.state.customers[customer].name}
            </option>)
        }

        let rRideOptions = [];
        let pRideOptions = [];
        for (let ride in this.state.rides) {
            if (this.state.rides[ride].status === 0) {
                rRideOptions.push(<option key={this.state.rides[ride].rideID} value={this.state.rides[ride].rideID}>
                        {this.state.rides[ride].rideID}
                    </option>)
            } else if (this.state.rides[ride].status === 1) {
                pRideOptions.push(<option key={this.state.rides[ride].rideID} value={this.state.rides[ride].rideID}>
                        {this.state.rides[ride].rideID}
                    </option>)
            }
        }
        
        let driverOptions = [];
        for (let driver in this.state.drivers) {
            driverOptions.push(<option key={this.state.drivers[driver].driverID} value={this.state.drivers[driver].driverID}>
                {this.state.drivers[driver].name}
            </option>)
        }

        return <>
            <div className="row mt-0 p-2 border rounded">
                <div className="d-flex flex-row justify-content-between"
                    style={{fontSize: "larger"}}>
                    <div className="d-flex flex-row align-items-center">
                        <div>
                            Customer: <select className="ms-1"
                                value={this.state.customer ? this.state.customer : ""}
                                onChange={(e) => { this.setState({customer: e.target.value}) }}
                                ref={this.cRef}>
                                {customerOptions}
                            </select>
                        </div>
                        <div className="mx-5">From: <input type="text" onChange={e => this.setState({from: e.target.value})}/></div>
                        <div>To: <input type="text" onChange={e => this.setState({to: e.target.value})}/></div>
                    </div>
                    <button className="btn btn-secondary" 
                        onClick={() => {
                            requestRide(this.cRef.current.value, this.state.from, this.state.to).then(() => { this.getRides() });
                        }}
                        disabled={!requestActive}>
                        Request
                    </button>
                </div>
            </div>
            <div className="row mt-2 p-2 border rounded">
                <div className="d-flex flex-row justify-content-between"
                    style={{fontSize: "larger"}}>
                    <div className="d-flex flex-row align-items-center w-100">
                        <div className="col">
                            Requested ride: <select className="ms-1"
                                value={this.state.requestRide ? this.state.requestRide : ""}
                                onChange={(e) => { this.setState({requestRide: e.target.value}) }}
                                ref={this.rRef}>
                                {rRideOptions}
                            </select>
                        </div>
                        <div className="col">
                            Driver: <select className="ms-1"
                                value={this.state.driver ? this.state.driver : ""}
                                onChange={(e) => { this.setState({driver: e.target.value}) }}
                                ref={this.dRef}>
                                {driverOptions}
                            </select>
                        </div>
                    </div>
                    <button className="btn btn-secondary"
                        onClick={() => {
                            let rid = this.rRef.current.value;
                            let ride = this.state.rides.filter(rideObj => rideObj.rideID === rid)[0];
                            assignRide(ride, this.dRef.current.value).then(() => { this.getDrivers() });
                        }}
                        disabled={!assignActive}>
                        Assign
                    </button>
                </div>
            </div>
            <div className="row mt-2 p-2 border rounded">
                <div className="d-flex flex-row justify-content-between"
                    style={{fontSize: "larger"}}>
                    <div className="d-flex flex-row align-items-center">
                        <div>
                            Ride in progress: <select className="ms-1"
                                value={this.state.progressRide ? this.state.progressRide : ""}
                                onChange={(e) => { this.setState({progressRide: e.target.value}) }}
                                ref={this.fRef}>
                                {pRideOptions}
                            </select>
                        </div>
                    </div>
                    <button className="btn btn-secondary"
                        onClick={() => {
                            let rid = this.fRef.current.value;
                            let ride = this.state.rides.filter(rideObj => rideObj.rideID === rid)[0];
                            finishRide(ride).then(() => { this.getDrivers() });
                        }}
                        disabled={!assignActive}>
                        Finish
                    </button>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col me-1 border rounded">
                    <CustomerList 
                        customers={this.state.customers}
                        clickedCallback={(id) => {
                            this.setState({customer: id});
                        }}/>
                </div>
                <div className="col ms-1 border rounded">
                    <DriverList 
                        drivers={this.state.drivers}
                        clickedCallback={(id) => {
                            this.setState({driver: id});
                        }}/>
                </div>
            </div>
            <div className="row mt-2 p-2 border rounded">
                <RideList 
                    rides={this.state.rides}
                    clickedCallback={(id) => {
                        let ride = this.state.rides.filter(r => r.rideID === id)[0];
                        if (ride.status === 0)
                            this.setState({requestRide: id});
                        else if (ride.status === 1)
                            this.setState({progressRide: id});
                    }}/>
            </div>
        </>;
    }
}
 
export default HomePage;