import React, { Component } from "react";
import CustomerList from "../lists/CustomerList";

class CustomerPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            customers: []
        }
    }

    getCustomers() {
        fetch("https://localhost:44382/api/customer", {
            method: "GET"
        })
        .then(res => res.json())
        .then((result) => {
            this.setState({
                customers: result
            });
        });
    }

    componentDidMount() {
        this.getCustomers();
    }

    render() { 
        return <>
            <div className="row mt-0 p-1 border rounded">
                <div className="d-flex flex-row justify-content-between"
                    style={{fontSize: "larger"}}>
                    Header
                </div>
            </div>
            <div className="row mt-2 p-2 border rounded">
                <CustomerList customers={this.state.customers} />
            </div>
        </>;
    }
}
 
export default CustomerPage;