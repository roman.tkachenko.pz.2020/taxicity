function RideList(props){
    let getStatus = (status) => {
        switch (status) {
            case 0: return "Requested";
            case 1: return "In progress";
            default: return "Done";
        }
    }

    let rides = []; 
    for (let ride in props.rides)
        rides.push(<tr key={props.rides[ride].rideID}
            onClick={() => props.clickedCallback(props.rides[ride].rideID)}>
            <td>{props.rides[ride].rideID}</td>
            <td>{props.rides[ride].customer}</td>
            <td>{props.rides[ride].driver}</td>
            <td>{props.rides[ride].start}</td>
            <td>{props.rides[ride].destination}</td>
            <td>{getStatus(props.rides[ride].status)}</td>
        </tr>);
    return <table className="table table-striped">
        <thead>
            <tr>
                <th>Ride ID</th>
                <th>Customer name</th>
                <th>Driver name</th>
                <th>Start address</th>
                <th>Destination address</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>{rides}</tbody>
    </table>;
}
 
export default RideList;