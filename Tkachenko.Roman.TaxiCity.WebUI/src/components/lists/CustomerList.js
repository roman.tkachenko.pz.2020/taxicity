function CustomerList(props){
    let cus = [];
    for (let cust in props.customers)
        cus.push(<tr key={props.customers[cust].customerID}
            onClick={() => props.clickedCallback(props.customers[cust].customerID)}>
            <td>{props.customers[cust].customerID}</td>
            <td>{props.customers[cust].name}</td>
        </tr>);
    return <table className="table table-striped">
        <thead>
            <tr>
                <th>Customer ID</th>
                <th>Customer name</th>
            </tr>
        </thead>
        <tbody>{cus}</tbody>
    </table>;
}
 
export default CustomerList;