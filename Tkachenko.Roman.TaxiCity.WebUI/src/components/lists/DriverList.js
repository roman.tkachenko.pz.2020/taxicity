function DriverList(props){
    let drivers = []; 
    for (let driver in props.drivers)
        drivers.push(<tr key={props.drivers[driver].driverID}
            onClick={() => props.clickedCallback(props.drivers[driver].driverID)}>
            <td>{props.drivers[driver].driverID}</td>
            <td>{props.drivers[driver].name}</td>
            <td>{props.drivers[driver].car}</td>
        </tr>);
    return <table className="table table-striped">
        <thead>
            <tr>
                <th>Driver ID</th>
                <th>Driver name</th>
                <th>Car</th>
            </tr>
        </thead>
        <tbody>{drivers}</tbody>
    </table>;
}
 
export default DriverList;