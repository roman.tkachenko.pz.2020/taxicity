import { NavLink } from "react-router-dom";

function Header(props) {
    return <div className="text-center d-flex flex-column justify-content-center mb-3">
        <h2>TaxiCity</h2>
        <nav className="d-flex flex-row justify-content-around">
            <PageReference name="Home" href="/"/>
            <PageReference name="Customers" href="/customers"/>
            <PageReference name="Drivers" href="/drivers"/>
        </nav>
    </div>;
}

function PageReference(props) {
    return (
        <NavLink 
            end
            className=
                {({ isActive }) => 
                    (isActive ? "active-page-ref page-ref w-100 p-3" : "page-ref w-100 p-3")} 
            to={props.href}>
            <div className="page-ref-container w-100">
                {props.name}
            </div>
        </NavLink>
    );
}
 
export default Header;