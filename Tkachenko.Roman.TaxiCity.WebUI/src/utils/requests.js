export async function getCustomer(id, setState) {
    fetch("https://localhost:44382/api/customer/" + id, {
            method: "GET"
        })
        .then(res => res.json())
        .then((result) => {
            setState({customer: result});
        });
}

export async function requestRide(customerID, from, to) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            customerID: customerID,
            start: from,
            destination: to,
            status: 0
        })
    };
    return await fetch('https://localhost:44382/api/ride/new', requestOptions);
}

export async function assignRide(ride, driverID) {
    ride.driverID = driverID;
    ride.status = 1;
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(ride)
    };
    return await fetch('https://localhost:44382/api/ride/update/' + ride.rideID, requestOptions);
}

export async function finishRide(ride) {
    ride.status = 2;
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(ride)
    };
    return await fetch('https://localhost:44382/api/ride/update/' + ride.rideID, requestOptions);
}