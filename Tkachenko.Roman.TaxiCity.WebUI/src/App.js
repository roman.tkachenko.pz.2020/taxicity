import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import {
  BrowserRouter as Router,
  Route,
  Routes
} from "react-router-dom";
import HomePage from "./components/pages/HomePage";
import Header from "./components/Header";
import CustomerPage from "./components/pages/CustomerPage";
import DriverPage from "./components/pages/DriverPage";

function App() {
  return (
    <div className="App">
      <Router>
        <Header/>
        <div className="container">
          <Routes>
              <Route exact path="/" element={<HomePage/>}/>
              <Route exact path="/customers" element={<CustomerPage/>}/>
              <Route exact path="/drivers" element={<DriverPage/>}/>
          </Routes>
        </div>
      </Router>
    </div>
  );
}

export default App;
